`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:38:18 09/18/2018 
// Design Name: 
// Module Name:    TestSegDec 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////////
//       A
//     /////
//   //     //
//  F//     //B
//   //  G  //
//     /////
//   //     //
//  E//     //C
//   //     //
//     /////    //H
//       D       
///////////////////////////////////////////////////////////////////////////////
module TestSegDec(
    input [3:0] dig,
    output reg [7:0] seg
    );

	always @(*)
		case(dig)        //HGFEDCBA
			4'd0:  seg = 8'b00111111;
			4'd1:  seg = 8'b00000110;
			4'd2:  seg = 8'b01011011;
			4'd3:  seg = 8'b01001111;
			4'd4:  seg = 8'b01100110;
			4'd5:  seg = 8'b01101101;
			4'd6:  seg = 8'b01111101;
			4'd7:  seg = 8'b00000111;
			4'd8:  seg = 8'b01111111;
			4'd9:  seg = 8'b01101111;
			4'd10: seg = 8'b01001111;
			4'd11: seg = 8'b01001111;
			4'd12: seg = 8'b01001111;
			4'd13: seg = 8'b01001111;
			4'd14: seg = 8'b01001111;
			4'd15: seg = 8'b01001111;	
		endcase
endmodule
