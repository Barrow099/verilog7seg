`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:16:21 09/18/2018 
// Design Name: 
// Module Name:    TestMux 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TestMux(
    input [1:0] s,
    input [15:0] data,
    output reg [3:0] dig
    );
	 
	 
	 always @(*)
		case(s)
			2'b00: dig = data[3:0];
			2'b01: dig = data[7:4];
			2'b10: dig = data[11:8];
			2'b11: dig = data[15:12];
		endcase
endmodule
