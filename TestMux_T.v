`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   20:20:55 09/18/2018
// Design Name:   TestMux
// Module Name:   D:/Programozas/Code/Verilog/TestMux/TestMux_T.v
// Project Name:  TestMux
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: TestMux
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module TestMux_T;

	// Inputs
	reg [1:0] s;
	reg [15:0] data;

	// Outputs
	wire [3:0] dig;
	

	// Instantiate the Unit Under Test (UUT)
	TestMux uut (
		.s(s), 
		.data(data), 
		.dig(dig)
	);

	initial begin
		// Initialize Inputs
		s = 0;
		data = 16'b1111_1010_1001_0001;

		// Wait 100 ns for global reset to finish
		#100;
		
		#50 s = 1;
		#50 s = 2;
		#50 s = 3;
		#50 s = 4;
		#50 s = 1;
		#50 s = 2;
		#50 s = 3;
		#50 s = 4;
		#50 s = 1;
		#50 s = 2;
		#50 s = 3;
		#50 s = 4;
		#50 s = 1;
		#50 s = 2;
		#50 s = 3;
		#50 s = 4;
		#50 s = 1;
		#50 s = 2;
		#50 s = 3;
		#50 s = 4;
		
        
		// Add stimulus here

	end
      
endmodule

