`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:29:27 09/18/2018 
// Design Name: 
// Module Name:    TestDec 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TestDec(
    input [1:0] s,
    output reg [3:0] data
    );

	always @(*)
		case (s)
			2'b00: data = 4'b0001;
			2'b01: data = 4'b0010;
			2'b10: data = 4'b0100;
			2'b11: data = 4'b1000;
		endcase
	

endmodule
