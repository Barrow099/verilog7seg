`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   21:31:17 09/18/2018
// Design Name:   TestDec
// Module Name:   D:/Programozas/Code/Verilog/TestMux/TestDec_T.v
// Project Name:  TestMux
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: TestDec
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module TestDec_T;

	// Inputs
	reg [1:0] s;

	// Outputs
	wire [3:0] data;

	// Instantiate the Unit Under Test (UUT)
	TestDec uut (
		.s(s), 
		.data(data)
	);

	initial begin
		// Initialize Inputs
		s = 0;

		// Wait 100 ns for global reset to finish
		#1000;
		
		#50 s = 1;
		#50 s = 2;
		#50 s = 3;
		#50 s = 4;
		#50 s = 1;
		#50 s = 2;
		#50 s = 3;
		#50 s = 4;
		#50 s = 1;
		#50 s = 2;
		#50 s = 3;
		#50 s = 4;
		#50 s = 1;
		#50 s = 2;
		#50 s = 3;
		#50 s = 4;
		#50 s = 1;
		#50 s = 2;
		#50 s = 3;
		#50 s = 4;
        
		// Add stimulus here

	end
      
endmodule

