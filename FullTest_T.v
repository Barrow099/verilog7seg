`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   21:59:25 09/18/2018
// Design Name:   FullDisp
// Module Name:   D:/Programozas/Code/Verilog/TestMux/FullTest_T.v
// Project Name:  TestMux
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: FullDisp
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module FullTest_T;

	// Inputs
	reg i_clock;
	reg [15:0] data;
	wire [3:0]  digit;
	wire [7:0] segment;
	// Instantiate the Unit Under Test (UUT)
	FullDisp uut (
		.i_clock(i_clock),
		.data(data),
		.digit(digit),
		.segment(segment)
	);
	
	

	initial begin
		// Initialize Inputs
		i_clock = 0;
		data = 16'b0001_1001_1001_1001;
		// Wait 100 ns for global reset to finish
		#100;
		
		forever begin
			#50 i_clock = i_clock + 1;
		end
        
		// Add stimulus here

	end
      
endmodule

