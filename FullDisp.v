`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:53:36 09/18/2018 
// Design Name: 
// Module Name:    FullDisp 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


module FullDisp(
    input i_clock,
	 input wire [15:0] data,
	 output reg [3:0] digit,
	 output reg [7:0] segment
    );
	 
	 reg [1:0] select;
	 wire [3:0] dig;
	 wire [3:0] dts;	
	 wire [7:0] segs;
	
	 TestMux mux(
		.s(select), 
		.data(data), 
		.dig(dig)
	  );
	 
	 TestDec dec(
		.s(select),
		.data(dts)
	 );
	 
	 TestSegDec segd(
		.dig(dig),
		.seg(segs)
	 );
	 
	 initial begin
		select = 2'b0;
		digit = 4'b0000;
	 end
	 
	 always @(posedge i_clock) begin
		select = select + 1;
		digit = dts;
		segment = segs;
	 end
	 
	


endmodule
